system-tools-backends (2.10.2-3) unstable; urgency=medium

  * Adding 06_fix_policy_user_root.patch (fix lintian warning).
  * Removing Uploaders from debian/control file due to adoption.
  * Bump Standards-Version to 3.9.8.
  * Adding 07_fix-ntp-servers.patch by Christophe Masson (Closes: #813925).

 -- Andriy Grytsenko <andrej@rep.kiev.ua>  Sat, 23 Apr 2016 20:27:51 +0300

system-tools-backends (2.10.2-2) unstable; urgency=low

  * New maintainer (Closes: #764264).

  [ Josselin Mouette ]
  * Update repository URL.

  [ jbicha-guest ]
  * use canonical Vcs-* fields

  [ Colin Watson ]
  * use dh_installdeb maintscript support (Closes: #659885).

  [ Vlad Orlov ]
  * 05_fix_selfconfig.patch: fixes current user's info retrieval.
    Closes: #761806.

  [ Andriy Grytsenko ]
  * debian/control, debian/copyright:
    - Bump Standards-Version to 3.9.6 (Fix lintian warning).
    - Change debian/copyright to a machine-readable format (for lintian).
  * debian/control:
    - Remove Vcs-* fields due to package adoption, new ones will be added later.
    - Add build dependency on autotools-dev to fix lintian warning
      outdated-autotools-helper-file.

 -- Andriy Grytsenko <andrej@rep.kiev.ua>  Fri, 10 Oct 2014 23:42:56 +0300

system-tools-backends (2.10.2-1) unstable; urgency=low

  * New upstream release.
  * debian/watch:
    - Track .bz2 tarballs.
  * Refresh debian/patches/04_empty_ntp.patch.
  * Bump debhelper compatibility level to 8.
    - Update Build-Depends on debhelper.
    - Strip debian/tmp/ from .install files.
  * debian/control.in:
   - Bump Standards-Version to 3.9.2. No further changes.
   - Update Vcs-* URLs.

 -- Michael Biebl <biebl@debian.org>  Sat, 22 Oct 2011 17:15:14 +0200

system-tools-backends (2.10.1-3) unstable; urgency=low

  [ Emilio Pozuelo Monfort ]
  * debian/patches/03_hostname.patch:
    - Forwarded upstream, add header.

  [ Josselin Mouette ]
  * 04_empty_ntp.patch: stolen from upstream git. Don’t create an empty 
    ntp.conf file. Closes: #397648.

  [ Jérémy Bobbio ]
  * Properly handle the rename of
    /etc/dbus-1/system.d/system-tools-backends.conf to
    org.freedesktop.SystemToolsBackends.conf.  (Closes: #553672)

  [ Josselin Mouette ]
  * Properly guard dpkg-maintscript-helper calls.

 -- Josselin Mouette <joss@debian.org>  Sun, 11 Sep 2011 10:55:32 +0200

system-tools-backends (2.10.1-2) unstable; urgency=low

  * 03_hostname.patch: patch by Mario Izquierdo to fix hostname 
    detection. Closes: #607152.

 -- Josselin Mouette <joss@debian.org>  Fri, 17 Dec 2010 22:48:37 +0100

system-tools-backends (2.10.1-1) unstable; urgency=low

  * New upstream bugfix release.
  * 01_kfreebsd.diff: add kfreebsd support. Closes: #594898.

 -- Josselin Mouette <joss@debian.org>  Wed, 20 Oct 2010 00:36:37 +0200

system-tools-backends (2.10.0-2) unstable; urgency=low

  * Break g-s-t and liboobs < 2.30 since the protocol has changed.

 -- Josselin Mouette <joss@debian.org>  Mon, 12 Apr 2010 14:53:17 +0200

system-tools-backends (2.10.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control.in:
    - Bump libpolkit-gobject-1-dev build-depend to 0.94.
    - Standards-Version is 3.8.4, no changes needed.
  * debian/patches/08_use_md5.patch:
    - Removed, dbus method no more returns whether to use md5 or not,
      password is now changed via PAM.
  * Switch to source format 3.0 (quilt).
    - Add debian/source/format.
    - Drop quilt from Build-Depends.
    - Remove /usr/share/cdbs/1/rules/patchsys-quilt.mk include.

 -- Luca Bruno <lethalman88@gmail.com>  Fri, 09 Apr 2010 13:18:21 +0200

system-tools-backends (2.8.3-1) unstable; urgency=low

  * New upstream bugfix release.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Fri, 18 Dec 2009 15:50:16 +0100

system-tools-backends (2.8.2-1) unstable; urgency=low

  * New upstream release.
    - debian/patches/01_debian_4.0.patch,
    - debian/patches/04_correct_perl_command.patch:
      + Removed, fixed upstream.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Thu, 08 Oct 2009 11:42:26 +0200

system-tools-backends (2.8.1-1) unstable; urgency=low

  [ Josselin Mouette ]
  * 09_check_etc_timezone.patch: patch from Ubuntu. Use /etc/timezone to 
    obtain time zone. Closes: #526940.

  [ Emilio Pozuelo Monfort ]
  * New upstream release.
    - debian/control.in:
      + Break gnome-system-tools << 2.28 because of the PolicyKit 1 migration.
      + Build depend on intltool.
      + Build depend on libpolkit-gobject-1-dev rather than
        libpolkit-dbus-dev for PolicyKit 1 support.
    - debian/patches/05_cve_2008_4311.patch,
      debian/patches/07_dont_symlink_localtime.patch,
      debian/patches/60_fix-permissions-of-pid-file.patch:
      + Removed, applied upstream.
    - debian/patches/09_check_etc_timezone.patch:
      + Removed, the new debian-4.0 definition that we use now has this.
    - debian/patches/01_debian_4.0.patch:
      + Refreshed.
    - debian/system-tools-backends.install:
      + system-tools-backends is now in /usr/sbin.
      + Update the location of the policykit stuff.
  * Use viewsvn in Vcs-Browser.
  * debian/patches/04_correct_perl_command.patch,
    debian/patches/08_use_md5.patch:
    - Add headers.
  * debian/control.in:
    - Unduplicate -dev long description.
    - Standards-Version is 3.8.3, no changes needed.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Sat, 03 Oct 2009 15:29:46 +0200

system-tools-backends (2.6.0-6.1) unstable; urgency=high

  * Security NMU.
  * Fix CVE-2008-6792 "limiting effective password length to 8 characters"
    and another related bug in do_get_use_md5(). Closes: #527952.

 -- Jan Christoph Nordholz <hesso@pool.math.tu-berlin.de>  Mon, 18 May 2009 17:55:01 +0200

system-tools-backends (2.6.0-6) unstable; urgency=low

  * 01_debian_4.0.patch: completely remove all the brain-dead version 
    logic. The Debian version corresponds to the version the package is 
    shipped in, that’s all.
  * Standards version is 3.8.1.
  * Point to versioned GPL in the copyright.

 -- Josselin Mouette <joss@debian.org>  Tue, 14 Apr 2009 21:27:06 +0200

system-tools-backends (2.6.0-5) unstable; urgency=low

  [ Michael Biebl ]
  * debian/control{.in}
    - Add Vcs-* fields.
    - Add Homepage field.
    - Add dependency on dbus (>= 1.1.2) which is required for D-Bus system bus
      activation (i.e. start-on-demand).

  [ Josselin Mouette ]
  * Remove dependency on adduser. Closes: #519366.
  * 01_debian_4.0.patch: update for the new string in 
    /etc/debian_version. Closes: #519773.
    + Make it also understand 6.0 in preparation for squeeze.

 -- Josselin Mouette <joss@debian.org>  Sat, 21 Mar 2009 16:03:05 +0100

system-tools-backends (2.6.0-4) unstable; urgency=low

  * 05_cve_2008_4311.patch: new patch, based on the patch by Simon 
    McVittie for the lenny Branch. Specify permissions with 
    send_destination instead of send_interface. Makes backends work with 
    the dbus packages fixing CVE-2008-4311. Closes: #510744.

 -- Josselin Mouette <joss@debian.org>  Wed, 11 Mar 2009 21:42:24 +0100

system-tools-backends (2.6.0-3) experimental; urgency=low

  [ Loic Minier ]
  * Don't rm_conffile /etc/dbus-1/event.d/70system-tools-backends during first
    configuration.

  [ Josselin Mouette ]
  * Build-depend on libpolkit-dbus-dev for PolicyKit support.
  * Make other build-dependencies up-to-date.
  * Break gnome-system-tools < 2.22.1-1.
  * 03_default_permissions.patch: removed, useless with PolicyKit.
  * Install the policy.
  * Remove the init script, the dispatcher is correctly started on 
    demand in the PolicyKit-enabled code path. Closes: #423903.
  * Remove the init script and symbolic links in the preinst.
  * system-tools-backends.1: fix typo. Closes: #482198.
  * system-tools-backends.postinst: removed, we don’t need stb-admin 
    anymore and we don’t need to reload dbus to make it start the 
    daemon.
  * Don’t pass --with-stb-group to the configure flags.

 -- Josselin Mouette <joss@debian.org>  Sun, 04 Jan 2009 11:50:44 +0100

system-tools-backends (2.6.0-2) unstable; urgency=medium

  [ Emilio Pozuelo Monfort ]
  * debian/patches/04_correct_perl_command.patch:
    - Use the system perl, thanks to James Westby and Sebastien Bacher.
      Closes: #488793.
  * debian/rules:
    - Cleanup
  * debian/control:
    - Updated Standards-Version to 3.8.0, no changes needed.

  [ Josselin Mouette ]
  * Switch to quilt for patch management; build-depend on quilt.
  * 07_dont_symlink_localtime.patch: patch from Ubuntu to support /etc 
    being on a partition separate from /usr. Thanks Steve Langasek and 
    James Westby. Closes: #488792.
  * 01_debian_4.0.patch: also support Debian 5.0 as lenny.
  * system-tools-backends.init: don't fail when STB is already running, 
    which can happen when dbus was upgraded in the same run and started 
    daemons using it. Closes: #469414.
  * Urgency medium for RC bug fix.

 -- Josselin Mouette <joss@debian.org>  Thu, 24 Jul 2008 10:58:16 +0200

system-tools-backends (2.6.0-1) unstable; urgency=low

  [ Loic Minier ]
  * New patch, 60_fix-permissions-of-pid-file, fixes permissions of .pid file;
    thanks Mike Bird; GNOME #533624; closes: #447361.

  [ Josselin Mouette ]
  * New upstream release.
  * 60_fix-permissions-of-pid-file.patch: fixed to apply again.
  * system-tools-backends.install: updated to match a directory change.
  * 03_default_permissions.patch: fix the default permissions so that 
    not all users are allowed to talk to the backends.
  * Conflict with liboobs-1-3.

 -- Josselin Mouette <joss@debian.org>  Wed, 28 May 2008 12:59:59 +0200

system-tools-backends (2.4.2-1) unstable; urgency=low

  [ Kilian Krause ]
  * Add get-orig-source target.
  * Fix preinst maintainer script. Add rm_conffile function from
    http://wiki.debian.org/DpkgConffileHandling. Actually this should be done
    through ucf, anyway.

  [ Sven Arvidsson ]
  * Add a basic man page (Closes: #433111)

  [ Loic Minier ]
  * Make system-tools-backends-dev arch: all as the /usr/lib/pkgconfig file is
    actually arch independent (and should be moved to /usr/share).

  [ Josselin Mouette ]
  * New upstream release. Closes: #453633.
  * 02ubuntu_chmod_network_interfaces_when_using_key.patch: updated from
    Ubuntu.
  * Define PIDDIR in the init script. Closes: #455600.
  * Entirely remove the old dbus script.
  * Standards version is 3.7.3.
  * Don’t stop the init script in the S runlevel.
  * Handle gracefully the case where the daemon fails to stop in the
    preinst.
  * Really remove the conffile by passing all arguments to rm_conffile.

 -- Josselin Mouette <joss@debian.org>  Sat, 16 Feb 2008 00:36:20 +0100

system-tools-backends (2.2.1-5) unstable; urgency=low

  the "Long Long due Release".
  * Migrated to use regular init as hinted by slomo AGES ago 
    (closes: #421722).
  * Add valid group.
  
 -- Niv Sardi <xaiki@debian.org>  Mon, 16 Jul 2007 10:37:49 +0200

system-tools-backends (2.2.1-4) unstable; urgency=low

  [ Niv Sardi ]
  * Add dependency on adduser, used during preinst.
    Thanks to Free Ekanayaka
    (closes: #424002: system-tools-backends: Missing dependency: adduser)

  [ Loic Minier ]
  * Fix typos in descriptions; thanks Paul Menzel; (closes: #425144).

  [ Sven Arvidsson ]
  * Fix unguarded use of script in preinst (closes: #425473, #423575).

  [ Otavio Salvador ]
  * Fix compilation options on rules.

 -- Otavio Salvador <otavio@debian.org>  Fri, 29 Jun 2007 11:31:29 -0300

system-tools-backends (2.2.1-3) unstable; urgency=low

  [ Niv Sardi]
  * Mark bugs to close.
  
  [ Loic Minier]
  * Re-add ubuntu version specific in preinst. :) (closes: #423575).
  * Wrap build-deps and deps.
  * Fix bashism in maintainer script.
  * Cleanups.
  * Add dep on libnet-dbus-perl; thanks Vincent Danjean; 
    (closes: #422702, #423596, #423620).

 -- Niv Sardi <xaiki@debian.org>  Mon, 14 May 2007 14:33:36 +0200

system-tools-backends (2.2.1-2) unstable; urgency=low

  * Upload to Unstable, remove check-dist.mk.
  * Remove ubuntu version specific in preinst.
  * Create stb-admin group in postinst (thanks to giskard).

 -- Niv Sardi <xaiki@debian.org>  Sat,  5 May 2007 16:07:03 +0200

system-tools-backends (2.2.1-1) experimental; urgency=low

  * New upstream release
  * Updated 01_debian_4.0.patch
  * Updated install files
  * Added build dep on libnet-dbus-perl
  * Changed from arch: all to any, as we now ship elf here. (thanks to lool)
  * Fixed Watch file. (thanks to lool)
  * build dep on gnome-pkg-tools 0.10 to use check-dist.mk. (thanks to lool)
  * New upstream release, purely bugfix release

  [ Imported from Ubuntu]
  * Imported patch 02ubuntu_chmod_network_interfaces_when_using_key.patch
  * Imported *.{post,pre}{inst,rm} and dbus init script.

 -- Niv Sardi <xaiki@debian.org>  Sat, 28 Apr 2007 12:54:25 +0200

system-tools-backends (1.4.2-3) unstable; urgency=medium

  * 01_debian_4.0.patch: support 4.0 as a Debian version, and drop 
    potato support (closes: #386778).

 -- Josselin Mouette <joss@debian.org>  Sun, 10 Sep 2006 11:21:32 +0200

system-tools-backends (1.4.2-2) unstable; urgency=low

  * Move debhelper, cdbs, and gnome-pkg-tools from Build-Depends-Indep to
    Build-Depends.
  * Bump up Standards-Version to 3.7.2.
  * Bump up Debhelper compatibility level to 5.

 -- Loic Minier <lool@dooz.org>  Sun, 13 Aug 2006 15:28:40 +0200

system-tools-backends (1.4.2-1) unstable; urgency=low

  * New upstream release.

 -- Jordi Mallach <jordi@debian.org>  Wed, 11 Jan 2006 11:29:27 +0100

system-tools-backends (1.4.1-1) unstable; urgency=low

  * New upstream release.
  * Updated debian/watch file.

 -- Ondřej Surý <ondrej@sury.org>  Fri, 30 Dec 2005 15:18:11 +0100

system-tools-backends (1.4.0-1) unstable; urgency=low

  * New upstream releases.
  * Add CDBS' utils. [debian/rules]
  * Update FSF address. [debian/copyright]
  * Distinguish Copyright from License. [debian/copyright]
  * Bump-up Standards-Version to 3.6.2. [debian/control, debian/control.in]

 -- Loic Minier <lool@dooz.org>  Wed, 14 Sep 2005 21:48:13 +0200

system-tools-backends (1.2.0-4) unstable; urgency=low

  * Don't forget to remove build dir...

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Sat, 18 Jun 2005 11:14:34 +0200

system-tools-backends (1.2.0-3) unstable; urgency=low

  * Fixed FTBFS because of missing builddep on 'gnome-pkg-tools'
    (my bad) (Closes: #314278).
  * Update POT file during build, needed for translators
    (Closes: #313528).
  * Now building in 'debian/build/' directory (cleaner).
  * Removed unnecessary DEB_CONFIGURE_SCRIPT_ENV in 'debian/rules'.

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Sat, 18 Jun 2005 11:07:09 +0200

system-tools-backends (1.2.0-2) unstable; urgency=low

  * Upload to unstable (GNOME Team upload).
  * Corrected 'system-tools-backends-dev' Section to 'devel'.

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Sat, 11 Jun 2005 00:53:59 +0200

system-tools-backends (1.2.0-1) experimental; urgency=low

  * Initial Release.
  * Marc Dequènes (Duck) did all the work for this release. Thanks!

 -- Jose Carlos Garcia Sogo <jsogo@debian.org>  Wed, 20 Apr 2005 23:07:33 +0200

